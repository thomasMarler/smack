import UIKit

class ChannelVC: UIViewController {

    @IBOutlet weak var loginBtn: UIButton!
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func loginBtnPressed(_ sender: Any) {
        
        //Segue to Login in VC using the TO_LOGIN segue
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
    }
    
}
