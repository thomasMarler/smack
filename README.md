# OverView #
Create a slack Clone with the help of Devslopes


# Home Page #
Home : ![Home](https://bytebucket.org/thomasMarler/smack/raw/db9cde827cb6028cbeb1e714aa8407666ec9ab5a/Picture/HomePage.png)


# Channel Page #
Channel : ![Channel](https://bytebucket.org/thomasMarler/smack/raw/db9cde827cb6028cbeb1e714aa8407666ec9ab5a/Picture/Channelpage.png)


# Login #

Login : ![Login Screen](https://bytebucket.org/thomasMarler/smack/raw/db9cde827cb6028cbeb1e714aa8407666ec9ab5a/Picture/Login.png)


# Create Account #
Create Account : ![Create Account](https://bytebucket.org/thomasMarler/smack/raw/692fb1aa945d4b4e98f7021a37fe99a25ad6e6b6/Picture/CreatedAccount.png)



# Pick Avatar #
Added Avatar : ![Added Avatar](https://bytebucket.org/thomasMarler/smack/raw/692fb1aa945d4b4e98f7021a37fe99a25ad6e6b6/Picture/AddedAvatar.png)
Avatar Light : ![Pick Avatar Light](https://bytebucket.org/thomasMarler/smack/raw/692fb1aa945d4b4e98f7021a37fe99a25ad6e6b6/Picture/AddedLightAvatar.png)
Avatar Dark : ![Pick Avatar Dark](https://bytebucket.org/thomasMarler/smack/raw/692fb1aa945d4b4e98f7021a37fe99a25ad6e6b6/Picture/ClickChooseAvatar.png)


# Database
Database : ![Database with User](https://bytebucket.org/thomasMarler/smack/raw/db9cde827cb6028cbeb1e714aa8407666ec9ab5a/Picture/Database.png)


### TODO ###
Fix Home Page
Socket Programming
Profile View
Display chat
Socket and Messages